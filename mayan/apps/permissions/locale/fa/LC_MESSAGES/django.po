# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Mehdi Amani <MehdiAmani@toorintan.com>, 2014,2018
# Mohammad Dashtizadeh <mohammad@dashtizadeh.net>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-10-29 12:44-0400\n"
"PO-Revision-Date: 2018-09-12 07:48+0000\n"
"Last-Translator: Roberto Rosario\n"
"Language-Team: Persian (http://www.transifex.com/rosarior/mayan-edms/language/fa/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: apps.py:32 models.py:28 models.py:78 permissions.py:7
msgid "Permissions"
msgstr "مجوزها"

#: classes.py:89
msgid "Insufficient permissions."
msgstr "اجازه ناکافی"

#: links.py:16 links.py:41 models.py:89 views.py:216
msgid "Roles"
msgstr "نقش ها"

#: links.py:20
msgid "Grant"
msgstr "دادن"

#: links.py:24
msgid "Revoke"
msgstr "لغو"

#: links.py:29
msgid "Create new role"
msgstr "ایجاد نقش role جدید"

#: links.py:33
msgid "Delete"
msgstr "حذف"

#: links.py:36
msgid "Edit"
msgstr "ویرایش"

#: links.py:44 models.py:81
msgid "Groups"
msgstr "گروه‌ها"

#: links.py:50
msgid "Role permissions"
msgstr "مجوزهای نقش"

#: models.py:19
msgid "Namespace"
msgstr "فضای نام"

#: models.py:20
msgid "Name"
msgstr "نام"

#: models.py:27
msgid "Permission"
msgstr "مجوز"

#: models.py:74 search.py:16
msgid "Label"
msgstr "برچسب"

#: models.py:88
msgid "Role"
msgstr "نقش"

#: permissions.py:10
msgid "View roles"
msgstr "دیدن نقش ها"

#: permissions.py:13
msgid "Edit roles"
msgstr "ویرایش نقش ها"

#: permissions.py:16
msgid "Create roles"
msgstr "ایجاد نقش ها"

#: permissions.py:19
msgid "Delete roles"
msgstr "حذف نقش ها"

#: permissions.py:22
msgid "Grant permissions"
msgstr "دادن مجوز"

#: permissions.py:25
msgid "Revoke permissions"
msgstr "لغو مجوز"

#: search.py:20
msgid "Group name"
msgstr "اسم گروه"

#: serializers.py:46
msgid ""
"Comma separated list of groups primary keys to add to, or replace in this "
"role."
msgstr "لیست کاملی از کلیدهای گروه اولیه برای اضافه کردن یا جایگزینی در این نقش جدا شده است."

#: serializers.py:53
msgid "Comma separated list of permission primary keys to grant to this role."
msgstr "لیست کلی از کلیدهای مجاز مجوز برای اعطای این نقش به یکدیگر جدا شده است."

#: serializers.py:90
#, python-format
msgid "No such permission: %s"
msgstr "این اجازه ئوجود ندارد: %s"

#: views.py:32
msgid "Available roles"
msgstr ""

#: views.py:33
msgid "Group roles"
msgstr ""

#: views.py:43
#, python-format
msgid "Roles of group: %s"
msgstr ""

#: views.py:85
msgid "Available groups"
msgstr "گروه های موجود"

#: views.py:86
msgid "Role groups"
msgstr "گروه های مختلف"

#: views.py:96
#, python-format
msgid "Groups of role: %s"
msgstr "گروه های نقش: %s"

#: views.py:98
msgid ""
"Add groups to be part of a role. They will inherit the role's permissions "
"and access controls."
msgstr ""

#: views.py:123
msgid "Available permissions"
msgstr "مجوزهای موجود"

#: views.py:124
msgid "Granted permissions"
msgstr "مجوزهای داده شده"

#: views.py:165
msgid ""
"Permissions granted here will apply to the entire system and all objects."
msgstr ""

#: views.py:168
#, python-format
msgid "Permissions for role: %s"
msgstr "مجوز برای نقش: %s"

#: views.py:208
msgid ""
"Roles are authorization units. They contain user groups which inherit the "
"role permissions for the entire system. Roles can also part of access "
"controls lists. Access controls list are permissions granted to a role for "
"specific objects which its group members inherit."
msgstr ""

#: views.py:215
msgid "There are no roles"
msgstr ""
